#include <Wire.h>
#include <Adafruit_BMP085.h>
Adafruit_BMP085 bmp;

void setup() {
  Serial.begin(9600);
  Wire.pins(0, 2);
  Wire.begin(0, 2);
  if (!bmp.begin()) {
    //   Serial.println("No BMP180 / BMP085");// we dont wait for this
    //   while (1) {}
  }

}

void loop() {
  String t = "T=" + String(bmp.readTemperature()) + " *C";
  String p = "P=" + String(bmp.readPressure()) + " Pa";
  String a = "A=" + String(bmp.readAltitude(103000)) + " m";// insert pressure at sea level
  Serial.println(t);
  delay(2000);
}
